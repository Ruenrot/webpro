<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cube &mdash; Free Website Template, Free HTML5 Template by gettemplates.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

		<nav class="gtco-nav" role="navigation">
			<div class="gtco-container">
				
				<div class="row">
					<div class="col-sm-2 col-xs-12">
						<div id="gtco-logo"><a href="index.html"><img src="images/logo.png" alt="Free HTML5 Website Template by GetTemplates.co"></a></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li><a href="index.php">Home</a></li>
							
							<li><a href="services.php">แบบบ้านทั้งหมด</a>
								
							</li>
							
							<li  ><a href="portfolio.php">รีวิว</a></li>
							<li class="active"><a href="contact.php">เข้าสู่ระบบ</a></li>
						</ul>
					</div>
				</div>
				
			</div>
		</nav>

	
		<div class="gtco-section">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>แก้ไขข้อมูล</h2>
					
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<form action="#">
							<div class="form-group">
								<label for="name">Username</label>
								<input type="text" class="form-control" id="user">
							</div>
							<div class="form-group">
								<label for="name">Password</label>
								<input type="text" class="form-control" id="pass">
							</div>
							<div class="form-group">
								<label for="name">ชื่อ</label>
								<input type="text" class="form-control" id="name">
							</div>
							<div class="form-group">
								<label for="name">เบอร์โทร</label>
								<input type="text" class="form-control" id="tel">
							</div>
							<div class="form-group">
								<label for="name">ที่อยู่</label>
								<input type="text" class="form-control" id="add">
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn btn-special" value="เข้าสู่ระบบ">
							</div>
						</form>
					</div>
					<div class="col-md-5 col-md-push-1">
						<div class="gtco-contact-info">
							<h3>Our Address</h3>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							<ul >
								<div class="form-group">
								<label for="name">Username</label>
								<input type="text" class="form-control" id="user">
							</div>
							<div class="form-group">
								<label for="name">Password</label>
								<input type="text" class="form-control" id="pass">
							</div>
							<div class="form-group">
								<label for="name">ชื่อ</label>
								<input type="text" class="form-control" id="name">
							</div>
							<div class="form-group">
								<label for="name">เบอร์โทร</label>
								<input type="text" class="form-control" id="tel">
							</div>
							<div class="form-group">
								<label for="name">ที่อยู่</label>
								<input type="text" class="form-control" id="add">
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn btn-special" value="เข้าสู่ระบบ">
							</div>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>

	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="js/google_map.js"></script>
	
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>