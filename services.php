<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cube &mdash; Free Website Template, Free HTML5 Template by gettemplates.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="gettemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

		<nav class="gtco-nav" role="navigation">
			<div class="gtco-container">
				
				<div class="row">
					<div class="col-sm-2 col-xs-12">
						<div id="gtco-logo"><a href="index.html"><img src="images/logo.png" alt="Free HTML5 Website Template by GetTemplates.co"></a></div>
					</div>
					<div class="col-xs-10 text-right menu-1">
						<ul>
							<li><a href="index.php">Home</a></li>
							
							<li class="active"><a href="services.php">แบบบ้านทั้งหมด</a>
								
							</li>
							
							<li ><a href="portfolio.php">รีวิว</a></li>
							<li><a href="contact.php">เข้าสู่ระบบ</a></li>
						</ul>
					</div>
				</div>
				
			</div>
		</nav>

		
		<header id="gtco-header" class="gtco-cover"style="background-image: url('images/b.jpg');" role="banner">
			<div class="gtco-container">
				<div class="row">
					<div class="col-md-12 col-md-offset-0 text-left">
						<div class="display-t">
							<div class="display-tc">
								<div class="row">
									<div class="col-md-8">
										<h1 class="no-margin">แบบบ้าน</h1>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- END #gtco-header -->



		<div class="gtco-section gtco-products">
			<div class="gtco-container">
				<div class="row row-pb-sm">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>แบบบ้านชั้นเดี่ยว</h2>
						
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-4 col-sm-4">
						<a href="home1.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/1059899-img.ssg489.0.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_1.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="home4.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/e66a47421fdeb38fc75773e50ae71324.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_2.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="home2.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/แบบบ้าน-106.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_md_1.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="home5.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/hqdefault.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_2.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="home3.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/HPM145A-1.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_3.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="home6.php" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/interhome-homez3-main.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_4.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					
				<!-- <div class="row">
					<div class="col-md-12 text-center">
					<p><a href="http://gettemplates.co" target="_blank" class="btn btn-special">Visit Gettemplates.co</a></p>
					</div>
				</div> -->
				<div class="row row-pb-sm">
					<div class="col-md-8 col-md-offset-2 gtco-heading text-center">
						<h2>แบบบ้าน2ชั้น</h2>
						
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-4 col-sm-4">
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/0135.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_1.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/h250820170956480.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_2.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/เชียงใหม่.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_md_1.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/thum-MTQ0NDcxMDQzNw==0.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_2.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					<div class="col-md-4 col-sm-4">
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/thum-MTUyNjI4NDM5NQ==0.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_3.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
						<a href="#" class="gtco-item two-row bg-img animate-box" style="background-image: url(images/oi.jpg)">
							<div class="overlay">
								<i class="ti-arrow-top-right"></i>
								<div class="copy">
									
								</div>
							</div>
							<img src="images/img_4.jpg" class="hidden" alt="Free HTML5 Website Template by GetTemplates.co">
						</a>
					</div>
					
		
		<!-- END .gtco-products -->

		
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

